USE ModernWays;
INSERT INTO Kledingstukken
VALUES
(1, 'polo', 'smal'),
(2, 'polo', 'medium'),
(3, 'polo', 'large'),
(4, 'broek', 'smal'),
(5, 'broek', 'medium'),
(6, 'broek', 'large'),
(7, 'trui', 'smal'),
(8, 'trui', 'medium'),
(9, 'trui', 'large');