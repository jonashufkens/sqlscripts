USE ModernWays;
CREATE TABLE Tweets(
Bericht_Id INT,
Id INT PRIMARY KEY AUTO_INCREMENT,
CONSTRAINT fk_Tweets_Bericht FOREIGN KEY (Bericht_Id) REFERENCES Bericht(Id)
);

CREATE TABLE Users(
Handle VARCHAR(144) CHAR SET utf8mb4 NOT NULL,
Id INT AUTO_INCREMENT PRIMARY KEY
);
